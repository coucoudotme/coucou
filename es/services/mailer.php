<?php
/* All form fields are automatically passed to the PHP script through the array $HTTP_POST_VARS. */
$data = json_decode(file_get_contents('php://input'), true);
$name = htmlspecialchars($data['name']);
$email = $data['email'];
$message = htmlspecialchars($data['message']);
$environment = "localhost"; //'coucou.me'

//Host information
$host_mail_address = "hi@coucou.me";
$subject = "$name, thanks for dropping a line.";

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
$headers .= 'To:'.$name.' <'.$email.'>'. "\r\n";
$headers .= 'From:'.$host_mail_address. "\r\n";
$headers .= 'Bcc:'.$host_mail_address. "\r\n";

//var_dump($_SERVER);
if(validate_email($email) == true ){
	if(mail($email, $subject, $message, $headers)) {
		echo "0";
	} else {
		echo "1";
	}
} else {
	echo "-1";
}

function validate_email($e){
    return (bool)preg_match("`^[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$`i", trim($e));
}
?>