<?php
    // Get image string posted from Android App
    $base=$_REQUEST['image'];

    $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base));

    // Get file name posted from Android App
    $filename = $_REQUEST['filename'];

    // Decode Image
    header('Content-Type: bitmap; charset=utf-8');

    $file = fopen('upload_image/'.$filename, 'wb');
    // Create File
    $fwrite = fwrite($file, $data);
    // close it
    fclose($file);    
    
    $response = array( 'result' => 1, 'message' => "so far so good" );

    if ($fwrite === false) {
        $response = array( 'result' => 0, 'message' => "something wrong just happened" );
    }

    //response to client
    header('Content-Type: application/json');
    echo json_encode( $response);
?>
